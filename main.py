""" Написать функцию, строящую дерево по списку пар id (id родителя, id потомка),# где None - id корневого узла.
Пример работы:
"""

source = [
    (None, 'a'),
    (None, 'b'),
    (None, 'c'),
    ('a', 'a1'),
    ('a', 'a2'),
    ('a2', 'a21'),
    ('a2', 'a22'),
    ('b', 'b1'),
    ('b1', 'b11'),
    ('b11', 'b111'),
    ('b', 'b2'),
    ('c', 'c1'),
]

expected = {
    'a': {'a1': {}, 'a2': {'a21': {}, 'a22': {}}},
    'b': {'b1': {'b11': {'b111': {}}}, 'b2': {}},
    'c': {'c1': {}},
}

def to_tree(source):
    result = {}
    branches = {}

    for parent_id, child_id in source:
        branch = result if parent_id is None else branches.setdefault(parent_id, {})
        branch[child_id] = branches.setdefault(child_id, {})
    return result
        
assert to_tree(source) == expected